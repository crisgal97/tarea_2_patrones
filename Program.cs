﻿/*

Funcionamiento del código:

El código proporciona una representación simplificada de una panadería, donde se definen diferentes tipos de productos de panadería, 
turnos de trabajo y sucursales.

La clase Gerente representa al gerente de la sucursal y tiene una propiedad Nombre para almacenar el nombre del gerente.
Las clases TurnoManana, TurnoTarde y TurnoNocturno representan los diferentes turnos de trabajo en la panadería y tienen una 
propiedad Nombre para almacenar el nombre del turno y una propiedad Panaderia para referenciar el tipo de panadería que se 
produce durante ese turno.
La clase abstracta Panaderia define el nombre y el tipo de producto de una panadería y tiene una propiedad abstracta Relleno 
para representar el relleno del producto (si aplica).
Las clases PanSaladoRellenoQueso, PanBaguette, GalletaChocolate y GalletaRellenaVainilla heredan de Panaderia y proporcionan 
implementaciones específicas para los diferentes tipos de productos de panadería. También especifican el tipo de producto y 
el tipo de relleno (si aplica).
La clase abstracta Relleno define el nombre del relleno de un producto de panadería y tiene una propiedad Nombre.
Las clases RellenoQueso, RellenoMermelada, RellenoPollo y RellenoVainilla heredan de Relleno y proporcionan implementaciones 
 para los diferentes tipos de rellenos.
La interfaz InterfazPan define un método MostrarInformacion() que muestra información específica para los productos de panadería 
que la implementen.
La clase Sucursal representa una sucursal de la panadería y tiene propiedades para almacenar el nombre, el teléfono, la dirección, 
el gerente y los turnos de trabajo de la sucursal. También tiene un método AsignarTurnos() para asignar los turnos de trabajo a la sucursal.
El programa principal (Program) crea instancias de las diferentes clases y establece las relaciones entre ellas. Luego, 
muestra información sobre una de las sucursales, incluyendo el nombre de la sucursal, los nombres de los turnos de trabajo y 
el tipo de panadería asociado a cada turno.

*/

using System;

namespace PanaderiaApp
{
    public class Program
    {
        public static void Main()
        {
            
            // Crear gerentes
            Gerente gerenteManana = new Gerente("Juan");
            Gerente gerenteTarde = new Gerente("Maria");

            // Crear turnos
            TurnoManana turnoManana = new TurnoManana();
            TurnoTarde turnoTarde = new TurnoTarde();
            TurnoNocturno turnoNocturno = new TurnoNocturno();

            // Crear panaderías
            PanSaladoRellenoQueso panSaladoRellenoQueso = new PanSaladoRellenoQueso();
            RellenoMermelada rellenoMermelada = new RellenoMermelada();
            RellenoPollo rellenoPollo = new RellenoPollo();
            PanBaguette panBaguette = new PanBaguette();
            GalletaChocolate galletaChocolate = new GalletaChocolate();
            GalletaRellenaVainilla galletaRellenaVainilla = new GalletaRellenaVainilla();

            // Crear sucursales
            Sucursal sucursal1 = new Sucursal("Sucursal 1", "123456789", "Dirección 1", gerenteManana);
            Sucursal sucursal2 = new Sucursal("Sucursal 2", "987654321", "Dirección 2", gerenteTarde);
            Sucursal sucursal3 = new Sucursal("Sucursal 3", "567891234", "Dirección 3", gerenteManana);

            // Asignar panaderías a los turnos
            turnoManana.Panaderia = panSaladoRellenoQueso;
            turnoTarde.Panaderia = panBaguette;
            turnoNocturno.Panaderia = galletaChocolate;

            // Asignar turnos a las sucursales
            sucursal1.AsignarTurnos(turnoManana, turnoTarde, turnoNocturno);
            sucursal2.AsignarTurnos(turnoManana, turnoTarde, turnoNocturno);
            sucursal3.AsignarTurnos(turnoManana, turnoTarde, turnoNocturno);

            
            Console.WriteLine("*******************************************************");
            Console.WriteLine("*               BIDD-02 - Patrones de diseño          *");
            Console.WriteLine("*                Universidad Cenfotec                 *");
            Console.WriteLine("*                       Tarea 2                       *");
            Console.WriteLine("*                                                     *");
            Console.WriteLine("*                    Profesor:                        *");
            Console.WriteLine("*        Lic. Jose Antonio Ortega González            *");
            Console.WriteLine("*                                                     *");
            Console.WriteLine("*                    Estudiante:                      *");
            Console.WriteLine("*               Cristian Murillo Mora                 *");
            Console.WriteLine("*                       2023                          *");
            Console.WriteLine("*                                                     *");
            Console.WriteLine("*          ¡Por favor, tenga piedad, profesor!        *");
            Console.WriteLine("*           Hice esto solo y me esforcé mucho.        *");
            Console.WriteLine("*******************************************************");


            // Imprimir información de la sucursal 1
            Console.WriteLine("Sucursal: " + sucursal1.Nombre);
            Console.WriteLine("Turno Mañana: " + sucursal1.TurnoManana.Nombre);
            Console.WriteLine("Turno Tarde: " + sucursal1.TurnoTarde.Nombre);
            Console.WriteLine("Turno Nocturno: " + sucursal1.TurnoNocturno.Nombre);

            Console.WriteLine("Panadería Turno Mañana: " + sucursal1.TurnoManana.Panaderia.Nombre);
            Console.WriteLine("Relleno Panadería Turno Mañana: " + sucursal1.TurnoManana.Panaderia.Relleno.Nombre);

            Console.WriteLine("Panadería Turno Tarde: " + sucursal1.TurnoTarde.Panaderia.Nombre);

            Console.WriteLine("Panadería Turno Nocturno: " + sucursal1.TurnoNocturno.Panaderia.Nombre);
        }
    }

    public class Gerente
    {
        public string Nombre { get; set; }

        public Gerente(string nombre)
        {
            Nombre = nombre ?? throw new ArgumentNullException(nameof(nombre));
        }
    }

    public class TurnoManana
    {
        public string Nombre { get; set; }
        public Panaderia Panaderia { get; set; }

        public TurnoManana()
        {
            Nombre = "Turno Mañana";
        }
    }

    public class TurnoTarde
    {
        public string Nombre { get; set; }
        public Panaderia Panaderia { get; set; }

        public TurnoTarde()
        {
            Nombre = "Turno Tarde";
        }
    }

    public class TurnoNocturno
    {
        public string Nombre { get; set; }
        public Panaderia Panaderia { get; set; }

        public TurnoNocturno()
        {
            Nombre = "Turno Nocturno";
        }
    }

    public abstract class Panaderia
    {
        public string Nombre { get; set; }
        public abstract string Tipo { get; }
        public abstract Relleno Relleno { get; }
    }

    public class PanSaladoRellenoQueso : Panaderia
    {
        public PanSaladoRellenoQueso()
        {
            Nombre = "Pan Salado Relleno de Queso";
        }

        public override string Tipo => "Pan Salado";

        public override Relleno Relleno => new RellenoQueso();
    }

    public class PanBaguette : Panaderia
    {
        public PanBaguette()
        {
            Nombre = "Pan Baguette";
        }

        public override string Tipo => "Pan Salado";

        public override Relleno Relleno => null;
    }

    public class GalletaChocolate : Panaderia
    {
        public GalletaChocolate()
        {
            Nombre = "Galleta de Chocolate";
        }

        public override string Tipo => "Galleta";

        public override Relleno Relleno => null;
    }

    public abstract class Relleno
    {
        public string Nombre { get; set; }
    }

    public class RellenoQueso : Relleno
    {
        public RellenoQueso()
        {
            Nombre = "Queso";
        }
    }

    public class RellenoMermelada : Relleno
    {
        public RellenoMermelada()
        {
            Nombre = "Mermelada";
        }
    }

    public class RellenoPollo : Relleno
    {
        public RellenoPollo()
        {
            Nombre = "Pollo";
        }
    }

    public class GalletaRellenaVainilla : Panaderia, InterfazPan
    {
        public GalletaRellenaVainilla()
        {
            Nombre = "Galleta Rellena de Vainilla";
        }

        public override string Tipo => "Galleta";

        public override Relleno Relleno => new RellenoVainilla();

        public void MostrarInformacion()
        {
            Console.WriteLine("Esta es una galleta rellena de vainilla.");
        }
    }

    public class RellenoVainilla : Relleno
    {
        public RellenoVainilla()
        {
            Nombre = "Vainilla";
        }
    }

    public interface InterfazPan
    {
        void MostrarInformacion();
    }

    public class Sucursal
    {
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public Gerente? Gerente { get; set; }
        public TurnoManana TurnoManana { get; set; }
        public TurnoTarde TurnoTarde { get; set; }
        public TurnoNocturno TurnoNocturno { get; set; }

        public Sucursal(string nombre, string telefono, string direccion, Gerente gerente)
        {
            Nombre = nombre;
            Telefono = telefono;
            Direccion = direccion;
            Gerente = gerente;
        }

        public void AsignarTurnos(TurnoManana turnoManana, TurnoTarde turnoTarde, TurnoNocturno turnoNocturno)
        {
            TurnoManana = turnoManana;
            TurnoTarde = turnoTarde;
            TurnoNocturno = turnoNocturno;
        }
    }
}
